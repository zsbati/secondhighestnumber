if __name__ == '__main__':
    n = int(input())
    arr = map(int, input().split())
    li = list(arr)
    
    def find_max(li):
        max = li[0]
        for i in range(len(li)):
            if max < li[i]:
                max = li[i]
        return max   

    def remove(li, item) :
        out = []
        for i in range(len(li)):
            if li[i] != item:
                out.append(li[i])
        return out

    m = find_max(li)

    arr1 = remove(li, m)

    print(find_max(arr1))

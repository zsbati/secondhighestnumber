arr = [1, 2, 3, 3, 4, 5, 5, 6, 4, 8]

def find_max(arr):
  max = arr[0]
  for i in range(len(arr)):
    if max < arr[i]:
      max = arr[i]
  return max   

def remove(arr, item) :
  out = []
  for i in range(len(arr)):
    if arr[i] != item:
      out.append(arr[i])
  return out

m = find_max(arr)

arr1 = remove(arr, m)

print(find_max(arr1))

